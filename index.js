//storing multiple values using variables
let student1 = '2020-01-17';
let student2 = '2020-01-12';
let student3 = '2020-01-21';
let student4 = '2020-01-17';

console.log(student1);
console.log(student2);
console.log(student3);

//storing multiple values in an array
const studentNumbers = ['2020-01-17', '2020-01-12', '2020-01-21', '2020-01-17'];
console.log(studentNumbers);

console.log(studentNumbers[0]);
console.log("Index 3 of studentNumbers", studentNumbers[5]);









//arrays are declared using the square brackets aka Array Literals
//each data stored inside an aray is called an array element

const emptyArray = [] //declared an empty array
const grades = [75, 85, 5, 92, 94]; // array of numbers
const computerBrands = ['Acer', 'Asus', 'Lenovo', 'Apple', 'Redfox', 'Gateway'];

console.log(emptyArray);
console.log(grades);
console.log(computerBrands);


const tasks = [
	'drink html',
	'eat JS',
	'inhale CSS',
	'bake sass'
];

console.log('Value of tasks before sort()', tasks);
tasks.sort();
console.log('Value of tasks after sort()', tasks);

const oddNumbs = [7, 11, 9, 15, 3, 1];

console.log('Value of oddNumbs before sort()', oddNumbs);
oddNumbs.sort();
console.log('Value of oddNumbs after sort()', oddNumbs);
oddNumbs.reverse();
console.log('Value of oddNumbs after reverse()', oddNumbs);

let fruits = [
	'Kiwi',
	'Banana',
	'Mango',
	'Coconut'

];
/*PUSH adds an element to the end of an array*/
console.log('Value of fruits array before push()' + fruits);
fruits.push('Orange');
console.log('Value of fruits array before push()' + fruits);

/* POP - removes last element in an array*/
console.log('Value of fruits array before pop()' + fruits);
fruits.pop();
console.log('Value of fruits array after pop()' + fruits);

/* UNSHIFT adds two or more element in the beginning of an array*/
console.log('Value of fruits array before unshift()' + fruits);
fruits.unshift('Santol', 'Strawberry');
console.log('Value of fruits array after unshift()' + fruits);

/*SHIFT() - removes the first element in our array */
console.log('Value of fruits array before shift()' + fruits);
let removedFruit = fruits.shift() //expected output is banana is removed from the fruits array
console.log("You successfully removed the fruit.", removedFruit);
console.log('Value of fruits array after shift()' + fruits);



const countries = ['US', 'PH', 'CAN', 'SG', 'PH', 'CAN'];
//indexOf() finds the index of a given element where it is FIRST found
let indexCountry = countries.indexOf('CAN');
console.log('Index of CAN', indexCountry);//1

//lastIndexOf() finds the index of a given element where it is LAST found
let lastIndexofCountry = countries.lastIndexOf('CAN');
console.log("Last Instance of CAN", lastIndexofCountry);
console.log(countries);

//toString() converts an array into a single value that is separated bya coma
console.log(countries.toString());

const subTasksA = ['drink html', 'eat JS'];
const subTasksB = ['inhale CSS', 'breathe sass'];
const subTasksC = ['get git', 'be node'];

//concat() joins 2 or more arrays
const subTasks = subTasksA.concat(subTasksB, subTasksC)
console.log(subTasks);

//join() converts an array into a single value, and is separated by a specified character
console.log(subTasks.join('-'));
console.log(subTasks.join('@'));
console.log(subTasks.join('x'));

let users = [
	'blue',
	'bianca',
	'alexis',
	'nikko',
	'adrian'

];
/*console.log(users[0]);
console.log(users[1]);*/

//syntax
/*
	arrayName.forEach(function(eachElement)) {
		console.log(eachElement) 
		}
	*/
//forEach iterates each element inside an array
//we pass each element as the parameter of the function declared inside the forEach()

//we have users array
//we want to iterate the users array using the forEach function
//logged in the browser the value of each user/each element inside the users array
users.forEach(function(user) {
	/*console.log('Name of Student: ', user);*/

	if (user == 'blue') {
		console.log('Name of Student: ', user);
	}


})

const numberList = [1, 2, 3, 4, 5, 6, 7, 8, 9];
numberList.forEach(function(numbers) {
if (numbers % 2 === 0) {
	console.log("All even numbers are: ", numbers);
}
})


/* MAP() function iterates each element and erturns new array with different values 
depending on the result of the function's operaetion*/

const numbersdata = numberList.map(function(number) {
	return number * number;
})
console.log(numbersdata);
console.log(numberList);//original array (will not be manipulated.)

console.log('numbers data before pop()', numbersdata);

numbersdata.pop();
console.log('numbers data after pop()',numbersdata);



const chessBoard = [
	['a1', 'b1','c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2','c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3','c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4','c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	


]
console.log(chessBoard);















let computerperipherals = [
	'mouse',
	'keyboard',
	'monitor',
	'headphones'

];
document.getElementById('array').innerHTML = computerperipherals;

function arrayPop() {
	computerperipherals.pop();
	console.log(computerperipherals);
	document.getElementById("array").innerHTML = computerperipherals;
	
}

function arrayShift() {
	computerperipherals.shift();
	console.log(computerperipherals);
	document.getElementById("array").innerHTML = computerperipherals;
	
}
function arrayPush() {
	if (computerperipherals.includes(document.getElementById('input').value)) {
		alert('Element is already in the array');
	}
	else if (document.getElementById('input').value === "") {
		alert('Fill in the textbox.');
	}
	else { 
	computerperipherals.push(document.getElementById('input').value.toLowerCase());
	document.getElementById("array").innerHTML = computerperipherals;
	}
	document.getElementById("input").value = "";

}

function arrayUnShift() {
if (computerperipherals.includes(document.getElementById('input').value)) {
		alert('Element is already in the array');
	}
	else if (document.getElementById('input').value === "") {
		alert('Fill in the textbox.');
	}
	else { 
	computerperipherals.unshift(document.getElementById('input').value.toLowerCase());
	document.getElementById("array").innerHTML = computerperipherals;
	}
	document.getElementById("input").value = "";



}